"use strict";

const nodemailer = require('nodemailer');
const nunjucks = require('nunjucks');
const Promise = require("bluebird");
const AModule = require("anna/AModule");

module.exports = class EmailerModule extends AModule {
    constructor (core, settings) {
        super(core, settings);

        this._nunjucks = null;
        this._transport = null;
    }

    init () {
        this.log.debug(`Email templates dir: ${this.settings.templates_dir}`);
        this._nunjucks = new nunjucks.Environment(new nunjucks.FileSystemLoader(this.settings.templates_dir, { watch: true }));

        this.log.debug(`Email transport module: ${this.settings.transport.library}`);
        const transportModule = require(this.settings.transport.library);

        this.log.debug(`Email transport module options: ${JSON.stringify(this.settings.transport.options)}`);
        this._transport = nodemailer.createTransport(transportModule(this.settings.transport.options));

        this.log.debug(`Email transport: ${JSON.stringify(this._transport)}`);

        return Promise.resolve(); 
    }

    sendEmail (template, templateContext, sendingOptions) {
        return new Promise((resolve, reject) => {
            if (!sendingOptions.html) {
                sendingOptions.html = this._nunjucks.render(template, templateContext);
            }

            if (!sendingOptions.from) {
                sendingOptions.from = this.settings.from;
            }

            if (!sendingOptions.to) {
                sendingOptions.to = this.settings.admins.join(", ");
            }

            this._transport.sendMail(sendingOptions, (err, info) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(info);
                }
            });
        });
    }
}
